<?php /*========================================
list
================================================*/ ?>
<div class="c-dev-title1">list</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list1</div>
<div style="background: url(/assets/img/common/bg1.jpg)">
    <div class="l-container">
        <div class="c-list1">
            <div class="c-list1__item">
                <p class="c-list1__number"><a href="#"><img src="/assets/img/common/num1.png" alt="1"></a></p>
                <div class="c-list1__title">
                    <h3>事前お申込キャンペーン</h3>
                </div>
                <p class="c-list1__subtitle pc-only">オープン日時日まで</p>
                <p class="c-list1__subtitle sp-only">フレオ-フン期間中</p>
                <div class="c-list1__txt">
                    <p><span class="c-list1__txt1">入会金・月会費</span><br><span class="fz32">４～５月分無料！</span></p>
                </div>
            </div>
            <div class="c-list1__item c-list1__item__type2">
                <p class="c-list1__number"><a href="#"><img src="/assets/img/common/num2.png" alt="2"></a></p>
                <div class="c-list1__title">
                    <h3>グランドオープン<br>入会キャンページ</h3>
                </div>
                <p class="c-list1__subtitle">2018<span class="fz12">年</span><span class="orange">4<span class="fz12">月中</span></span></p>
                <div class="c-list1__txt">
                    <p>
                        入会金<span class="c-list1__txt2">無料！</span>
                        <br>４～５月分<span class="c-list1__txt2 pc-only">3,000月<span>（税抜）</span>!!</span><span class="c-list1__txt2 sp-only">3,000月<span>（税抜）</span>!</span>
                    </p>
                </div>
            </div>
            <div class="c-list1__item c-list1__item__type2">
                <p class="c-list1__number"><a href="#"><img src="/assets/img/common/num3.png" alt="3"></a></p>
                <div class="c-list1__title">
                    <h3>グランドオープン<br><span>体験レッスンキャンページ</span></h3>
                </div>
                <p class="c-list1__subtitle">2018<span class="fz12">年</span><span class="orange">4<span class="fz12">月中</span></span></p>
                <div class="c-list1__txt">
                    <p class="pc-only">体験レッスン料<span class="c-list1__txt2">無料！</span></p>
                    <p class="sp-only">体験レッスン<span class="c-list1__txt2">無料！</span></p>
                </div>
            </div>
        </div>
    </div>
</div>